import axios, { AxiosInstance } from "axios";
import { ConversionPrice, Quotes } from "../types/coinmarket";
import dotenv from "dotenv";

dotenv.config();

export class CoinMarket {
  baseURL: string = "https://pro-api.coinmarketcap.com/v1";
  axiosInstance: AxiosInstance;

  constructor() {
    const apiKeyHeader: string | undefined = process.env.API_KEY;

    if (apiKeyHeader !== undefined) {
      this.axiosInstance = axios.create({
        baseURL: this.baseURL,
        method: "GET",
        headers: { "X-CMC_PRO_API_KEY": apiKeyHeader },
      });
    } else {
      throw new Error("Missing API KEY in environment variable");
    }
  }

  async quotes(symbol: Array<string>): Promise<Quotes> {
    const paramsToString = symbol.toString();

    let request = {
      params: { symbol: paramsToString },
    };

    const response = await this.axiosInstance.get(
      "/cryptocurrency/quotes/latest",
      request
    );
    return response.data as Quotes;
  }

  async conversion(
    amount: number,
    symbol: string,
    convert: Array<string>
  ): Promise<ConversionPrice> {
    const paramsToString = symbol.toString();

    let request = {
      params: {
        symbol: symbol,
        amount: amount,
        convert: paramsToString,
      },
    };

    const response = await this.axiosInstance.get(
      "/tools/price-conversion",
      request
    );
    return response.data as ConversionPrice;
  }
}
