export interface Quote {
  price: number;
  last_updated: string;
}

export interface Quotes {
  id: number;
  name: string;
  symbol: string;
  slug: string;
  date_added: string;
  last_updated: string;
  quote: Quote;
}

export interface ConversionPrice {
  id: number;
  symbol: string;
  name: string;
  amount: number;
  last_updated: string;
  quote: Quote;
}
